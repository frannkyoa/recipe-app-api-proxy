# recipe-app-api-proxy

NGINX proxy app for Recipe App API Proxy Applicaiton

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen (default: `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)